Source: spectemu
Section: contrib/otherosfs
Priority: optional
Maintainer: Colin Watson <cjwatson@debian.org>
Standards-Version: 3.5.6
Build-Depends:
 debhelper-compat (= 13),
 dh-exec,
 libreadline-dev,
 libx11-dev,
 libxext-dev,
 libxt-dev,
 x11proto-core-dev,
Vcs-Git: https://salsa.debian.org/debian/spectemu.git
Vcs-Browser: https://salsa.debian.org/debian/spectemu
X-Style: black

Package: spectemu-common
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 spectemu-x11,
Replaces:
 xspectemu,
Description: Fast 48k ZX Spectrum Emulator (common files)
 Spectemu emulates the 48k ZX Spectrum, which uses the Z80 microprocessor.
 This package contains common configuration files and utilities which are or
 can be used by either the X11 or the SVGAlib frontend.
 .
 It emulates the Z80 processor as well as the 48k Spectrum's other
 hardware: keyboard, screen, sound, tape I/O. The emulation is very
 close to the real thing, but it is still quite fast (It was reported
 to be working well on a laptop with 486 at 25MHz!). On the other hand,
 the user interface is not the best.
 .
 Features include:
    - Sound support through Linux kernel sound-card driver.
    - Snapshot saving and loading (.Z80 and .SNA format)
    - Tape emulation: loading from tape files (.TAP and .TZX format)
    - Optional quick loading of tapes.
    - Saving to tape files.
    - Separate utility to save tape files to real tape
    - Configurable with config files and from command line

Package: spectemu-x11
Architecture: any
Depends:
 spectemu-common (= ${binary:Version}),
 spectrum-roms,
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts:
 xspectemu,
Replaces:
 xspectemu,
Provides:
 xspectemu,
Description: Fast 48k ZX Spectrum Emulator for X11
 xspect is the X11 version of Spectemu which emulates the 48k ZX
 Spectrum, which uses the Z80 microprocessor.
 .
 It emulates the Z80 processor as well as the 48k Spectrum's other
 hardware: keyboard, screen, sound, tape I/O. The emulation is very
 close to the real thing, but it is still quite fast (It was reported
 to be working well on a laptop with 486 at 25MHz!). On the other hand,
 the user interface is not the best.
 .
 Features include:
    - Sound support through Linux kernel sound-card driver.
    - Snapshot saving and loading (.Z80 and .SNA format)
    - Tape emulation: loading from tape files (.TAP and .TZX format)
    - Optional quick loading of tapes.
    - Saving to tape files.
    - Separate utility to save tape files to real tape
    - Configurable with config files and from command line
